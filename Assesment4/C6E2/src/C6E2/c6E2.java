package C6E2;

import javax.swing.JOptionPane;

public class c6E2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = JOptionPane.showInputDialog("Some Characters");
		char ch[] = str.toCharArray();
		int x = ch.length-1, numVowl = 0;
		while (x >= 0 ) {
			if (isVowel(ch[x]) == true)
				numVowl++;
				x--;
		}
		JOptionPane.showMessageDialog(null,"There where " + numVowl + " vowels");
		
	}
	
	public static boolean  isVowel(char x) {
		char ch = x;
		
		if (ch == 'a' || ch == 'A'|| ch == 'e' || ch == 'E'
				|| ch == 'i' || ch == 'I' || ch == 'o' ||
				ch == 'O' || ch == 'u' || ch == 'U' || 
				ch == 'y' || ch == 'Y') {
			return true;
		}else {
			return false;
			
		}
		
	}

}
