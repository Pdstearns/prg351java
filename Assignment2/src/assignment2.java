import javax.swing.JOptionPane;

public class assignment2 {
	public static double sum(double x, double y) {
		double first = x;
		double second = y;
		double result = first + second;
		return result;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String string;
		double first, second, result;
		string = JOptionPane.showInputDialog("Input a number");
		first = Double.parseDouble(string);
		string = JOptionPane.showInputDialog("Input another Number");
		second = Double.parseDouble(string);
		result = sum(first,second);
		JOptionPane.showMessageDialog(null, "The Sum of those numbers is " + result, "Sum Result ", JOptionPane.INFORMATION_MESSAGE);
	}

}
