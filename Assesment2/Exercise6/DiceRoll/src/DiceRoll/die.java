package DiceRoll;

public class die {
	private int num;
	
	
	public die() {
		num = 1;
	}
	
	public die(int x) {
		num = x;
	}
	
	public double getNum() {
		return this.num;
	}
	public void roll() {
		num = (int)(Math.random()*6) + 1;
	}
}
