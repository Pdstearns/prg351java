package C5E2;

import javax.swing.JOptionPane;

public class c5E2 {

	public static void main(String[] args) {
		String str = JOptionPane.showInputDialog("Enter and Integer");
		char num[] = str.toCharArray();
		int x = 0, y = str.length()-1;
		while (x <= y) {
			char temp = num[x];
			num[x] = num[y];
			num[y] = temp;
			x++;
			y--;
		}
		str = new String(num);
		JOptionPane.showMessageDialog(null, "Reversed that integer is " + str);
	}

}
